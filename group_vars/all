
debug_mode:                      false                                # Set this variable as true if you want some debug info when runing

### tool  ###
tool_vm_provider:                "libvirt"                            # VM provider
tool_memory:                     "256"                                # Amount of RAM (Mo)
tool_cpus:                       "1"                                  # Number of processors (Minimum value of 2 otherwise it will not work)
tool_keymap:                     "fr"                                 # Keymap 
tool_image_name:                 "debian/buster64"                    # Image to use
tool_number:                     "1"                                  # Number of VM to create
tool_hostname_begin:             "kub-"                               # String to use as begin of the hostname
tool_hostname_ending:            "-tool"                              # String to use as end   of the hostname

### master ###
master_vm_provider:              "libvirt"                            # VM provider
master_memory:                   "2048"                               # Amount of RAM (Mo)
master_cpus:                     "2"                                  # Number of processors (Minimum value of 2 otherwise it will not work)
master_keymap:                   "fr"                                 # Keymap
master_image_name:               "debian/buster64"                    # Image to use
master_number:                   "1"                                  # Number of VM to create
master_hostname_begin:           "kub-"                               # String to use as begin of the hostname
master_hostname_ending:          "-master"                            # String to use as end   of the hostname

### worker ###
worker_vm_provider:              "libvirt"                            # VM provider
worker_memory:                   "2048"                               # Amount of RAM (Mo)
worker_cpus:                     "2"                                  # Number of processors (Minimum value of 2 otherwise it will not work)
worker_keymap:                   "fr"                                 # Keymap
worker_image_name:               "debian/buster64"                    # Image to use
worker_number:                   "1"                                  # Number of worker node
worker_hostname_begin:           "kub-"                               # String to use as begin of the hostname
worker_hostname_ending:          "-worker"                            # String to use as end   of the hostname

# Network parameters
vagrant_node_network_base:       "192.168.100"                        # First three octets of the IP address that will be assign to all type of k8s cluster nodes
vagrant_node_network_base_inv:   "100.168.192"                        # Idem but inversed for the dns configuration
vagrant_pod_network:             "10.244.0.0/16"                      # Private network for inter-pod communication

vagrant_node_ip:                 "{{ vagrant_node_network_base }}.{{ tool_number|int + 2 }}"                        # ip   of the first master node
vagrant_node_name:               "{{ master_hostname_begin }}{{ tool_number|int + 1 }}{{ master_hostname_ending }}" # name of the first master node

domain_name:                     "k8s-land.net"                       # Domain name
netmask:                         "255.255.255.0"                      # Netmask
ip_dns_server:                   "{{ vagrant_node_network_base }}.2"  # IP of the dns server
ip_dhcp_server:                  "{{ vagrant_node_network_base }}.2"  # IP of the dhcp server
dns_forwarder_1:                 "208.67.222.222"                     # IP of the first  dns forwarder to use (opendns)  
dns_forwarder_2:                 "208.67.220.220"                     # IP of the second dns forwarder to use (opendns) 

# user
k8s_user:                        "vagrant"                            # user who acces all VM to to administre the k8s cluster 
