
&nbsp;
&nbsp;

&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# yomykube 

This project generate a kubernetes cluster on a local computer
- create VM (libvirt) with vagrant: 
  - n°1: tool (dns server, etc.)
  - n°2: kubernetes cluster master node
  - n°3: kubernetes cluster worker node

To show the result of the automation: 

ssh vagrant@192.168.100.3

kubectl get nodes -o wide

```console
NAME           STATUS   ROLES    AGE   VERSION   INTERNAL-IP     EXTERNAL-IP   OS-IMAGE                       KERNEL-VERSION   CONTAINER-RUNTIME
kub-2-master   Ready    master   80s   v1.18.6   192.168.100.3   <none>        Debian GNU/Linux 10 (buster)   4.19.0-9-amd64   docker://19.3.12
kub-3-worker   Ready    worker   32s   v1.18.6   192.168.100.4   <none>        Debian GNU/Linux 10 (buster)   4.19.0-9-amd64   docker://19.3.12
```



&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;


&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# Instructions

&nbsp;
&nbsp;

## prerequisite 
- internet connection
- a computer under debian 10 operating system (not tested on ubuntu)


&nbsp;
&nbsp;

## preparation


Run thoses commands
```console


# install prerequisites packages
sudo apt install ansible git -y  

# get the code to use
git clone https://gitlab.com/stephane.gambus/yomykube.git

# enter in the directory downloaded
cd yomykube

# generate ssh key
./bin/initial.sh

```


&nbsp;
&nbsp;

## check/set parameters  

check/set parameters on [./yomykub/group_vars/all](https://gitlab.com/stephane.gambus/yomykube/blob/master/group_vars/all)




&nbsp;
&nbsp;


## run the automation

run the command:
```console
make
```


**note:** to remove all VM, do:     ./bin/vagrant_destroy.sh

**note:** to see all VM, do:        vagrant global-status   or   virt-manager




&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;

&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;&#x1F535;
# Quality


the command **ansible-lint playbook.yml** return 0 warnings



&nbsp;
&nbsp;

