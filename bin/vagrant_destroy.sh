#!/bin/bash

vagrant global-status | grep libvirt | colrm 8 | xargs -L 1 -t vagrant halt

vagrant destroy -f
